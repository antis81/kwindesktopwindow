# KWin Desktop Window Test Application
This application reproduces the "show desktop" issue described in [kde-bug 442044](https://bugs.kde.org/show_bug.cgi?id=442044).

## License
Do whatever you want License!

## Build Instructions

**important note**: Build from command line!

```bash
mkdir build
cd build
cmake -G='Ninja' ..
ninja
# or with good old make
# cmake ..
# make

# run
./KWinDestkopWindow
```

**Do not use any IDE to build this because it will(!) confuse window management and behave different.**
