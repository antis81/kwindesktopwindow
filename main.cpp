#include <QApplication>
#include <QLabel>
#include <QMainWindow>
#include <QTimer>

// Important note:
// Compile this app from commandline - IDE will confuse window management!
// --
// mkdir build
// cd build
// cmake ..
// make
// --

QWidget* desktopCreate(int width=1000, int height=1000) {
  auto w = new QMainWindow();
  w->setWindowFlags(Qt::FramelessWindowHint);
  // INFO: For kwin(ft) this makes the difference!
  w->setAttribute(Qt::WA_X11NetWmWindowTypeDesktop);
  w->setAttribute(Qt::WA_DeleteOnClose);
  w->resize(width, height);
  new QLabel(QStringLiteral("Desktop"), w);
  return w;
}

QWidget* windowCreate(QString text, int width=250, int height=250) {
  auto w = new QMainWindow();
  w->setAttribute(Qt::WA_DeleteOnClose);
  w->resize(width, height);
  new QLabel(text, w);
  return w;
}

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  // create the desktop window
  auto desk = desktopCreate();
  desk->show();

  // create another window after QEventLoop started
  QTimer::singleShot(1000, [desk]{
    auto w = windowCreate(QStringLiteral("Window 1"));
    auto deskCenterX = desk->x() + desk->width() / 2;
    auto deskCenterY = desk->y() + desk->height() / 2;
    QPoint deskCenter{ deskCenterX, deskCenterY };
    w->move(deskCenter - w->geometry().center());
    w->show();
  });

  return a.exec();
}
